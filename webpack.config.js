'use strict';

const _configs = {
    // global section
    global: require(__dirname + '/config/webpack/global'),

    // config by enviroments
    production: require(__dirname + '/config/webpack/environments/production'),
    development: require(__dirname + '/config/webpack/environments/development')
};

const _load = function() {
    let env = process.env.NODE_EN || 'production';

    // env = 'production';
    // env = 'development';

    return mergeDeep(
        _configs[env](__dirname),
        _configs.global(__dirname)
    );
};

const isObject = item => {
    return (item && typeof item === 'object' && !Array.isArray(item) && item !== null);
};

const mergeDeep = (target, source) => {
    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, { [key]: {} });
                mergeDeep(target[key], source[key]);
            } else {
                Object.assign(target, { [key]: source[key] });
            }
        }
    }

    return target;
};

module.exports = _load();
