'use strict';

module.exports = function(_path) {
    const rootAssetPath = _path + 'app';

    return {
        context: _path,
        devtool: 'eval',
        devServer: {
            contentBase: './dist',
            hot: false,
            inline: true
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    enforce: 'pre',
                    use: 'eslint-loader'
                },
                // {
                //     test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                //     use: 'url-loader'
                // },
                // {
                //     test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                //     use: 'file-loader'
                // },
                // {
                //     test: /\.(css|png|ico|jpg|jpeg|gif)$/i,
                //     use: ['file-loader?context=' + rootAssetPath + '&name=assets/static/[ext]/[name].[hash].[ext]']
                // }
            ]
        }
    };
}
