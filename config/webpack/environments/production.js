'use strict';

module.exports = function(_path) {
    const rootAssetPath = _path + 'app';

    return {
        context: _path,
        devtool: 'cheap-source-map',
        output: {
            publicPath: '/public/'
        },
        // module: {
        //     rules: [
        //         {
        //             test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        //             use: 'url-loader'
        //         },
        //         {
        //             test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        //             use: 'file-loader?name=assets/static/[path][name].[ext]'
        //         },
        //         {
        //             test: /\.(css|png|ico|jpg|jpeg|gif)$/i,
        //             use: ['file-loader?context=' + rootAssetPath + 'name=assets/static/[path]/[name].[ext]']
        //         }
        //     ]
        // }
    }
}