'use strict';

const path = require('path');
const webpack = require('webpack');
const Manifest = require('manifest-revision-webpack-plugin');
const TextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const HtmlPlugin = require('html-webpack-plugin');

module.exports = function(_path) {
    // define local variables
    const npmPackages = require(_path + '/package');
    const dependencies = (npmPackages.dependencies) ? Object.keys(npmPackages.dependencies) : false;
    const rootAssetPath = _path + 'app';

    const bootstraprcCustomLocation =  path.join(_path, 'config', '.bs3');
    const bootstrapProdEntryPoint = 'bootstrap-loader/lib/bootstrap.loader?extractStyles' +
                                    `&configFilePath=${bootstraprcCustomLocation}` +
                                    '!bootstrap-loader/no-op.js';

    // define entry points
    const entryPoints = {
        application: _path + '/app/app.js'
    };

    entryPoints.vendors = [
        'font-awesome-loader',
        'jquery',
        bootstrapProdEntryPoint
    ];

    return {
        // entry points
        entry: entryPoints,

        // output system
        output: {
            path: path.join(_path, 'dist'),
            filename: path.join('assets', 'js', '[name].[hash].js'),
            chunkFilename: '[id].bundle.[chunkhash].js',
            publicPath: '/'
        },

        // resolves modules
        resolve: {
            extensions: ['.js'],
            modules: ['node_modules'],
            alias: {
                _svg: path.join(_path, 'app', 'assets', 'svg'),
                _fonts: path.join(_path, 'app', 'assets', 'fonts'),
                _modules: path.join(_path, 'app', 'modules'),
                _images: path.join(_path, 'app', 'assets', 'images'),
                _stylesheets: path.join(_path, 'app', 'assets', 'stylesheets'),
                _templates: path.join(_path, 'app', 'assets', 'templates')
            }
        },

        // modules resolvers
        module: {
            rules: [
                {
                    test: /\.jade$/,
                    use: 'jade-loader?pretty'
                },
                {
                    test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: 'url-loader'
                },
                {
                    test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                    use: ['file-loader?context=' + rootAssetPath + '&&name=assets/bootstrap/fonts/[name].[hash].[ext]'],
                    exclude: [
                        /logo\.svg/
                    ]
                    // test: /bootstrap-sass/,
                    // use: 'file-loader'
                },
                {
                    test: /\.(css|png|ico|jpg|jpeg|gif|svg)$/i,
                    use: ['file-loader?context=' + rootAssetPath + '&&name=assets/static/[ext]/[name].[hash].[ext]']
                },
                {
                    test: /bootstrap-sass\/assets\/javascripts\//,
                    use: 'imports-loader?jQuery=jquery'
                }
            ]
        },

        // load plugins
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendors',
                filename: 'assets/js/vendors.[hash].js'
            }),
            new TextPlugin('assets/css/[name].[chunkhash].css'),
            new webpack.LoaderOptionsPlugin({
                options: {
                  postcss: [
                    autoprefixer({ browsers: ['last 5 versions'] })
                  ]
                }
            }),
            new Manifest(path.join(_path + '/config', 'manifest.json'), {
                rootAssetPath: rootAssetPath
            }),

            new HtmlPlugin({
                title: 'Продажа и ремонт компьютерной техники',
                chunks: ['application', 'vendors'],
                filename: 'index.html',
                template: path.join(_path, 'app', 'assets', 'templates', 'pages', 'index.jade')
            }),
            new HtmlPlugin({
                title: 'Контакты',
                chunks: ['application', 'vendors'],
                filename: 'contacts.html',
                template: path.join(_path, 'app', 'assets', 'templates', 'pages', 'contacts.jade')
            }),
            new HtmlPlugin({
                title: 'Партнёры',
                chunks: ['application', 'vendors'],
                filename: 'partners.html',
                template: path.join(_path, 'app', 'assets', 'templates', 'pages', 'partners.jade')
            }),
            new HtmlPlugin({
                title: 'Партнёры',
                chunks: ['application', 'vendors'],
                filename: 'about.html',
                template: path.join(_path, 'app', 'assets', 'templates', 'pages', 'about.jade')
            })
        ]
    };
};
